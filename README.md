# Backport Composer

A very lightweight module that registers Composer's Autoloader in Drupal 7.

This module is primarily to support the [Backport](https://www.drupal.org/project/backport)
module as the preferred or "native" Composer solution in Drupal 7, similar to
what is now done in Drupal 8.

This is a standalone implementation and not a comprehensive solutions like
[Composer Manager](https://www.drupal.org/project/composer_manager).

### Installation

1. Copy the module into your `sites/all/modules` folder.
2. Go to `/admin/modules` and enable the `Backport Composer` in the `Utilities`
   section (or if you use Drush: `drush en backport_composer`).

### Setup

By default, this module should immediately work "out-of-the-box" as it attempts
to automatically locate the first viable `autoload.php` file it can find.

The locations in which it searches depends greatly on how the site is
configured and which modules are used.

There are five "root" paths this module checks:

1. Top level (indicates project level management):
   - `DRUPAL_ROOT`
   - `DRUPAL_ROOT/..` (one level outside `DRUPAL_ROOT`)
2. Specific site files (whatever the variable `file_default_scheme` is set to):
   - `public://`
3. Specific site directory:
   - `sites/example.com` or `sites/default` as a fallback
4. All sites directory:
   - `sites/all`

Finally, for each of the above `ROOT` paths, this module will check if
`autoload.php` exists at one of the following locations:

- `ROOT/autoload.php`
- `ROOT/vendor/autoload.php`
- `ROOT/composer/autoload.php`
- `ROOT/composer/vendor/autoload.php`
- `ROOT/composer_manager/autoload.php`
- `ROOT/composer_manager/vendor/autoload.php`

This module supports [Composer Manager](https://www.drupal.org/project/composer_manager)
locations solely for the express purpose of easier transitions or migrations
to a "native" composer setup.

### Configuration

If you have Composer installed at a different location there are a couple ways
you can customize which autoloader is used:

1. Create a custom file in the root of your site (e.g. `./DOCROOT/autoload.php`)
   and then return the require of the Composer's `autoload.php` file from
   whichever location you need (similar to what Drupal 8 does):
   ```php
   <?php
   return require '../../../sites/example.com/files/vendor/autoload.php';
   ```
2. Set the following variable in your site's `settings.php` file to a path
   where Composer's `autoload.php` file resides:
   ```php
   <?php
   $conf['backport_composer_autoload_path'] = DRUPAL_ROOT . '/../../../sites/example.com/files/vendor/autoload.php';
   ```


### Notes

1. The location of the Composer `autoload.php` file is cached, even if set as
   a variable in your `settings.php` or `settings.local.php` file. This is to
   ensure that an absolute path is always ascertained in the event the variable
   uses relative positioning or contains any symbolic links. If its location
   changes, you will need to clear the database cache for it to be
   rediscovered. This is primarily to reduce unnecessary file system checks
   on each bootstrap.
2. If you create a custom `autoload.php` which requires another `autoload.php`,
   you should always explicitly use `require` instead of `require_once` as the
   latter will return `TRUE` instead of the autoloader if it has already been
   required in PHP (which is possibility).
3. If the variable `backport_composer_autoload_path` is set, it will always
   take precedence over any automated discovery if it is a valid path. If it
   isn't, it will fallback to automated discovery.
4. If the variable `page_cache_without_database` is set, it is also highly
   recommended that you explicitly set the `backport_composer_autoload_path`
   variable as not doing so may cause more than one intended file system check
   to determine the absolute path of the `autoload.php` file as any cached
   value stored in the database will not be retrievable.
5. If the variable `page_cache_invoke_hooks` has been disabled, Composer's
   `autoload.php` will not be invoked or registered. This is a limitation of
   how page caching works and you will need to figure out a way to circumvent
   this and manually invoke `backport_composer_autoloader()` if its
   functionality is desired at this step of the bootstrap phase. 
